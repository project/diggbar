# Installation and Usage

There isn't anything tricky to this module. 

* Checkout the module within your sites/(all|default)/modules directory

        git clone git://github.com/mannkind/diggbar-drupal-module.git diggbar

* Enable the module with Drupal
* (Optionally) Change the Diggbar message presented to users

By default this module will show John Gruber's message:
> Dear Digg,
>
> Framing sites is bullshit.
>
> Your pal,
>
> --J.G.

**NOTE:** This module will not work with Drupal's Aggressive Cache setting.  
Sorry! You'll need to use some kind of Javascript Framebuster script for that!
